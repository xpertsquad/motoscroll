<?php
// +------------------------------------------------------------------------+
// | @author Deen Doughouz (DoughouzForest)
// | @author_url 1: http://www.wowonder.com
// | @author_url 2: http://codecanyon.net/user/doughouzforest
// | @author_email: wowondersocial@gmail.com   
// +------------------------------------------------------------------------+
// | WoWonder - The Ultimate PHP Social Networking Platform
// | Copyright (c) 2016 WoWonder. All rights reserved.
// +------------------------------------------------------------------------+
// MySQL Hostname
$sql_db_host = "localhost";
// MySQL Database User
$sql_db_user = "root";
// MySQL Database Password
$sql_db_pass = "root";
// MySQL Database Name
$sql_db_name = "motoscroll";

// Site URL
$site_url = "http://localhost/motoscroll"; // e.g (http://example.com)

// Purchase code
$purchase_code = "1bdbaac4-2315-4d5b-b005-e5d93b6cf3ef"; // Your purchase code, don't give it to anyone. 
?>